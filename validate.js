function DomID(id) {
    return document.getElementById(id)
}
function showMessage(idSpan,message) {
    DomID(idSpan).style.display = "block"
    DomID(idSpan).innerText= message

}
var validator = {
    ktrDodai : function(value,min,max,idSpan) {
        var length = value.length
        if (length>=min && length<=max) {
            showMessage(idSpan,"")
            return true
        }
        else {
            showMessage(idSpan,`Độ dài phải từ ${min}-${max}`)
            return false
        }
    },
    ktrTruonRong : function(value,idSpan) {
        if (value == "") {
            showMessage(idSpan,"*Trường này không được để trống")
            return false
        }
       else {
        showMessage(idSpan,"")
        return true
       }
    },
    ktrKhoangTrang : function (value,idSpan){
        var regex = /^[a-zA-Z0-9_.-]*$/
        if (regex.test(value)) {
            showMessage(idSpan,"")
            return true
        }
        else{
            showMessage(idSpan,"*Tài khoản không được chứa khoảng cách và ký tượng đặc biệt")
            return false
        }
    },
    ktrTrungTaikhoan : function (value,listNV,idSpan) {
       var index = listNV.findIndex(function(item) {
            return value == item.taikhoan
        })
        if (index!=-1){
            showMessage(idSpan,"*Tài khoản này đã tồn tại")
            return false
        }
        else{
            showMessage(idSpan,"")
            return true
        }
    },
    ktrChu : function(value,idSpan){
     var regex = /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
        if (regex.test(value)) {
            showMessage(idSpan,"")
            return true
        } else {
            showMessage(idSpan,"*Tên phải là chữ")
            return false
        }
    },
    ktrEmail:function(value,idSpan) {
        const re =
        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
        if (re.test(value)) {
            showMessage(idSpan,"")
            return true
        }
        else{
            showMessage(idSpan,"*Email chưa đúng định dạng")
            return false
        }
    },
    ktrMatKhau : function (value,idSpan) {
        var regex =
        /^(?=.*[A-Z])(?=.*[0-9])(?=.*[-._!"`'#%&,:;<>=@{}~\$\(\)\*\+\/\\\?\[\]\^\|])/;
        if (regex.test(value)) {
            showMessage(idSpan,"")
            return true
        }
        else {
            showMessage(idSpan,"*Mật khẩu phải chứa ít nhất 1 ký tự số 1 ký tự chữ in hoa và 1 ký tự đặc biệt")
            return false
        }
    },
    ktrNgayThang : function(value,idSpan) {
        var regex = /^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/;
        if (regex.test(value)) {
            showMessage(idSpan,"")
            return true
        }
        else {
            showMessage(idSpan,"*Ngày Tháng chưa đúng định dạng")
            return false
        }

    },
    ktrLuong : function (value,idSpan){
        if (value>=1e6 && value<=20e6) {
         showMessage(idSpan,"")   
         return true
        }
        else {
            showMessage(idSpan,"*Số lương phải từ 1000000 đến 20000000")
            return false
        }
    },
    ktrGio : function (value,idSpan){
        if (value>=80 && value<=200) {
         showMessage(idSpan,"")   
         return true
        }
        else {
            showMessage(idSpan,"*Số giờ phải từ 80 đến 200")
            return false
        }
    },
    ktrChucvu : function(value,idSpan) {
        if (value=="0") {
            showMessage(idSpan,"*Phải Chọn 1 trong 3")
            return false
        }
        else {
            showMessage(idSpan,"")
            return true
        }
    }
}
function validateTaikhoan()  {
    var taikhoan = DomID("tknv").value
    var isValid =  validator.ktrTruonRong(taikhoan,"tbTKNV") &&
    validator.ktrKhoangTrang(taikhoan,"tbTKNV") && 
    validator.ktrDodai(taikhoan,4,6,"tbTKNV") &&
    validator.ktrTrungTaikhoan(taikhoan,listNV,"tbTKNV")
    return isValid
}
function validateName() {
    var name = DomID("name").value
    var isValid = validator.ktrTruonRong(name,"tbTen") && 
    validator.ktrChu(name,"tbTen")
    return isValid
}
function validateEmail () {
    var email = document.getElementById("email").value
    var isValid =validator.ktrTruonRong(email,"tbEmail") && validator.ktrEmail(email,"tbEmail")
    return isValid
}
function validateMatKhau() {
    var matkhau = DomID("password").value
    var isValid = validator.ktrTruonRong(matkhau,"tbMatKhau")&& validator.ktrDodai(matkhau,6,10,"tbMatKhau") && validator.ktrMatKhau(matkhau,"tbMatKhau")
    return isValid
}
function validateNgayLam() {
    var date = DomID("datepicker").value
    var isValid = validator.ktrTruonRong(date,"tbNgay") && validator.ktrNgayThang(date,"tbNgay")
    return isValid
}
function validateLuong () {
    var luong = DomID("luongCB").value
    var isValid = validator.ktrTruonRong(luong,"tbLuongCB") && validator.ktrLuong(luong,"tbLuongCB")
    return isValid 

}
function validateChucVu() {
    var chucVu = DomID("chucvu").value
    var isValid = validator.ktrChucvu(chucVu,"tbChucVu") 
    return isValid
}
function validateGio() {
    var gio = DomID("gioLam").value
    var isValid = validator.ktrTruonRong(gio,"tbGiolam") && validator.ktrGio(gio,"tbGiolam") 
    return isValid
}
function isValidator () {
    var isValidator = 
    validateTaikhoan() &
    validateName() &
    validateEmail() &
    validateMatKhau() &
    validateNgayLam() &
    validateLuong() &
    validateChucVu() &
    validateGio()
    if (isValidator) {
        return true
    }
    else {
        return false
    }
}



