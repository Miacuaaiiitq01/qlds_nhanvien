function showThongTinNhanVien(listNV) {
    var content = "";
    for (var i = 0; i < listNV.length; i++) {
      nv = listNV[i];
      content += `<tr>
      <td>${nv.taikhoan}</td>
      <td>${nv.ten}</td>
      <td>${nv.email}</td>
      <td>${nv.date}</td>
      <td>${nv.chucVu()}</td>
      <td>${nv.TongLuong().toLocaleString()}</td>
      <td>${nv.XepLoai()}</td>
      <td><Button class="btn btn-danger" onclick="XoaNhanVien('${nv.taikhoan}')">Xóa</Button></td>
      <td><Button class="btn btn-danger" onclick="SuaNhanVien('${nv.taikhoan}')" data-toggle="modal"
      data-target="#myModal">Sửa</Button></td>
     </tr>`;
    }
    document.getElementById("tableDanhSach").innerHTML = content
  }
  function layThongTinTuform () {
      var taikhoan = DomID("tknv").value
      var ten = DomID("name").value
      var email = DomID("email").value
      var pass = DomID("password").value
      var date = DomID("datepicker").value
      var luongCB = DomID("luongCB").value
      var valueChucVu = DomID("chucvu").value
      var sogiolam = DomID("gioLam").value
      var NV = new NhanVien(taikhoan,ten,email,pass,date,luongCB,valueChucVu,sogiolam)
      return NV
  
  }
  function showTable(nv) {
    DomID("tknv").value = nv.taikhoan
    DomID("name").value =  nv.ten
    DomID("email").value =  nv.email 
    DomID("password").value =  nv.pass
    DomID("datepicker").value =  nv.date
    DomID("luongCB").value =  nv.luongCB
    DomID("chucvu").value =  nv.valueChucVu
    DomID("gioLam").value =  nv.sogiolam
  }
  