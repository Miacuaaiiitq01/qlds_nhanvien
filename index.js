listNV = [];
var showData = localStorage.getItem("DATA");

if (showData !== null) {
  listNV = JSON.parse(showData).map(function (nv) {
    return new NhanVien(
      nv.taikhoan,
      nv.ten,
      nv.email,
      nv.pass,
      nv.date,
      nv.luongCB,
      nv.valueChucVu,
      nv.sogiolam
    );
  });
  showThongTinNhanVien(listNV);
}

function themNhanVien() {
  var NV = layThongTinTuform();
  if (isValidator()) {
    listNV.push(NV);
    showThongTinNhanVien(listNV);
    var data = JSON.stringify(listNV);
    localStorage.setItem("DATA", data);
  }
}
function XoaNhanVien(ma) {
  console.log(ma);
  var index = listNV.findIndex(function (item) {
    return ma == item.taikhoan;
  });
  listNV.splice(index, 1);
  showThongTinNhanVien(listNV);
  var data = JSON.stringify(listNV);
  localStorage.setItem("DATA", data);
}
//
function SuaNhanVien(taikhoan) {
  DomID("tknv").disabled = true;
  var index = listNV.findIndex(function (item) {
    return taikhoan == item.taikhoan;
  });
  showTable(listNV[index]);
}
function CapNhatSinhVien() {
  var nv = layThongTinTuform();

  var index = listNV.findIndex(function (item) {
    return item.taikhoan == nv.taikhoan;
  });
  listNV[index] = layThongTinTuform();
  if (
    validateName() &
    validateEmail() &
    validateMatKhau() &
    validateNgayLam() &
    validateLuong() &
    validateChucVu() &
    validateGio()
  ) {
    showThongTinNhanVien(listNV);
    var data = JSON.stringify(listNV);
    localStorage.setItem("DATA", data);
  }
}

DomID("btnThem").onclick = function () {
  DomID("tknv").disabled = false;
  DomID("my-form").reset();
};
